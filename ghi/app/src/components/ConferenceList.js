import { useEffect, useState } from 'react';

function ConferenceList() {
    const [conferences, setConferences] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8000/api/conferences/');

        if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    return(
    <table className="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Location</th>
                <th>ID</th>
            </tr>
        </thead>
        <tbody>
        {conferences?.map(conference => {
            return (
                <tr key={conference.href}>
                    <td>{conference.name}</td>
                    <td>{conference.location}</td>
                    <td>{conference.id}</td>
                </tr>
            );
        })}
        </tbody>
    </table>
    )
}
export default ConferenceList;
