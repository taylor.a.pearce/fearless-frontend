import React, {useEffect, useState} from 'react'

function AttendConferenceForm() {

    const [conferences, setConferences] = useState([])

    const [formData, setFormData] = useState({
        email: '',
        name: '',
        company_name: '',
        conference: '',
    })

    const fetchData = async () => {
        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const response = await fetch(conferenceUrl)

        if (response.ok) {
            const data = await response.json()
            setConferences(data.conferences)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const attendeeUrl = 'http://localhost:8001/api/conferences/1/attendees/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(attendeeUrl, fetchConfig)
        if (response.ok) {
            setFormData({
                email: '',
                name: '',
                company_name: '',
                conference: '',
            })
            const newAttendee = await response.json()
            console.log(newAttendee)
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value
        const inputName = event.target.name
        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    return (
        <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new attendee</h1>
                    <form onSubmit={handleSubmit} id="create-attendee-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Email" required type="email" value={formData.email} name="email" id="email" className="form-control"/>
                        <label htmlFor="email">Email</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Name" required type="text" value={formData.name} name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Company Name" type="text" value={formData.company_name} name="company_name" id="company_name" className="form-control"/>
                        <label htmlFor="company_name">Company name</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} required id="conference" value={formData.conference} name="conference" className="form-select">
                        <option>Choose a conference</option>
                        {conferences.map(conference => {
                            return (
                                <option key={conference.id} value={conference.id}>
                                    {conference.name}
                                </option>
                            )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>

    )
}

export default AttendConferenceForm
