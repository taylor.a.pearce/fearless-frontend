import Nav from './Nav';
import AttendeesList from './components/AttendeesList';
import LocationForm from './components/LocationForm';
import ConferenceForm from './components/ConferenceForm'
import ConferenceList from './components/ConferenceList';

import AttendConferenceForm from './components/AttendConferenceForm';
import MainPage from './MainPage';

import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom"



function App() {
  return (

    <BrowserRouter>
      <Nav />
      <div className="container">
      <Routes>
        <Route index element={<MainPage />} />

        <Route path="attendees">
          <Route path="new" element={<AttendConferenceForm />} />
          <Route path="list" element={<AttendeesList />} />
        </Route>

        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
          <Route index element={<ConferenceList />} />
        </Route>

        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>

      </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
